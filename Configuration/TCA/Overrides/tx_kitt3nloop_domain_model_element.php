<?php

$sModel = basename(__FILE__, '.php');

$GLOBALS['TCA'][$sModel]['types']['1']['showitem'] = str_replace('variant,', '', $GLOBALS['TCA'][$sModel]['types']['1']['showitem']);

$sColumn = 'link';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'input',
    'renderType' => 'inputLink',
];

$sColumn = 'icon';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['type'] = 'select';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['renderType'] = 'selectSingle';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['items'] = [
    ['--', ''],
];
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['size'] = 1;

$sColumn = 'nav_icon';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['type'] = 'select';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['renderType'] = 'selectSingle';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['items'] = [
    ['--', ''],
];
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['size'] = 1;