<?php

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'Kitt3n | Loop',
        'kitt3nloop_elements',
        'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/kitten_482x482.svg'
    ),
    'CType',
    'kitt3n_loop'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'Kitt3n | Loop',
        'kitt3nloop_elements_accordion',
        'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/kitten_482x482.svg'
    ),
    'CType',
    'kitt3n_loop'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'Kitt3n | Loop',
        'kitt3nloop_elements_boxes',
        'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/kitten_482x482.svg'
    ),
    'CType',
    'kitt3n_loop'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'Kitt3n | Loop',
        'kitt3nloop_elements_events',
        'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/kitten_482x482.svg'
    ),
    'CType',
    'kitt3n_loop'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'Kitt3n | Loop',
        'kitt3nloop_elements_images',
        'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/kitten_482x482.svg'
    ),
    'CType',
    'kitt3n_loop'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'Kitt3n | Loop',
        'kitt3nloop_elements_slider',
        'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/kitten_482x482.svg'
    ),
    'CType',
    'kitt3n_loop'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'Kitt3n | Loop',
        'kitt3nloop_elements_tabs',
        'EXT:kitt3n_brand/Resources/Public/Assets/IconRegistry/SVGs/kitten_482x482.svg'
    ),
    'CType',
    'kitt3n_loop'
);

$sModel = basename(__FILE__, '.php');
$GLOBALS['TCA'][$sModel]['ctrl']['typeicon_classes']['kitt3nloop_elements'] = 'kitt3n_svg_big';
$GLOBALS['TCA'][$sModel]['ctrl']['typeicon_classes']['kitt3nloop_elements_accordion'] = 'kitt3n_svg_big';
$GLOBALS['TCA'][$sModel]['ctrl']['typeicon_classes']['kitt3nloop_elements_boxes'] = 'kitt3n_svg_big';
$GLOBALS['TCA'][$sModel]['ctrl']['typeicon_classes']['kitt3nloop_elements_events'] = 'kitt3n_svg_big';
$GLOBALS['TCA'][$sModel]['ctrl']['typeicon_classes']['kitt3nloop_elements_images'] = 'kitt3n_svg_big';
$GLOBALS['TCA'][$sModel]['ctrl']['typeicon_classes']['kitt3nloop_elements_slider'] = 'kitt3n_svg_big';
$GLOBALS['TCA'][$sModel]['ctrl']['typeicon_classes']['kitt3nloop_elements_tabs'] = 'kitt3n_svg_big';

$sModel = basename(__FILE__, '.php');
$sColumn = 'tx_kitt3nloop_domain_model_element';
$sTable = 'tx_kitt3nloop_domain_model_element';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'group',
    'internal_type' => 'db',
    'allowed' => $sTable,
    'foreign_table' => $sTable,
    'foreign_table_where' => 'AND sys_language_uid IN (-1,0) AND deleted = 0 AND hidden = 0',
    'MM' => 'ttcontent_tx_kitt3nloop_domain_model_element_mm',
    'size' => 10,
    'minitems' => 0,
    'maxitems' => 10,
    'multiple' => 1,
    'suggestOptions' => [
        'default' => [
            'searchWholePhrase' => 1,
        ],
        $sTable => [
            'searchCondition' => 'sys_language_uid IN (-1,0)'
        ],
    ],
];

$sModel = basename(__FILE__, '.php');
$sColumn = 'image';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['maxitems'] = 1;

$sColumn = 'tx_kitt3nloop_template';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'select',
    'renderType' => 'selectSingle',
    'items' => [
        ['Default', ''],
        ['Variant 1', '1'],
        ['Variant 2', '2'],
        ['Variant 3', '3'],
    ],
    'fieldWizard' => [
        'selectIcons' => [
            'disabled' => false,
        ],
    ],
];

// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements'] = [
    'showitem' => '
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
            tx_kitt3nloop_domain_model_element;Kitt3n Loop Element(s),
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
        tx_kitt3nloop_template;Kitt3n Loop Template,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
            rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
    ',
    'columnsOverrides' => [
        'bodytext' => [
            'config' => [
                'enableRichtext' => true,
                'richtextConfiguration' => 'default'
            ]
        ]
    ]
];

$GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements_accordion']['showitem'] =
    $GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements']['showitem'];

$GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements_boxes']['showitem'] =
    $GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements']['showitem'];

$GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements_events']['showitem'] =
    $GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements']['showitem'];

$GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements_images']['showitem'] =
    $GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements']['showitem'];

$GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements_slider']['showitem'] =
    $GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements']['showitem'];

$GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements_tabs']['showitem'] =
    $GLOBALS['TCA']['tt_content']['types']['kitt3nloop_elements']['showitem'];