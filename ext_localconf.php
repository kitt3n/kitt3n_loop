<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
defined('TYPO3_MODE') || die('Access denied.');

$boot = function () {

    ## Add page tsconfig for ce_box
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
'mod {
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        kitt3nloop_elements {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Loop | Elements | Loop
                            description = Create generic loop elements
                            tt_content_defValues {
                                CType = kitt3nloop_elements
                            }
                        }
                    }
                    show := addToList(kitt3nloop_elements)
                }
            }
            
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        kitt3nloop_elements_accordion {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Loop | Elements | Accordion
                            description = Create an accordion
                            tt_content_defValues {
                                CType = kitt3nloop_elements_accordion
                            }
                        }
                    }
                    show := addToList(kitt3nloop_elements_accordion)
                }
            }
            
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        kitt3nloop_elements_boxes {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Loop | Elements | Boxes
                            description = Create boxes
                            tt_content_defValues {
                                CType = kitt3nloop_elements_boxes
                            }
                        }
                    }
                    show := addToList(kitt3nloop_elements_boxes)
                }
            }
            
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        kitt3nloop_elements_events {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Loop | Elements | Events
                            description = Create a list od events
                            tt_content_defValues {
                                CType = kitt3nloop_elements_events
                            }
                        }
                    }
                    show := addToList(kitt3nloop_elements_events)
                }
            }
            
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        kitt3nloop_elements_images {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Loop | Elements | Images
                            description = Create a list of images
                            tt_content_defValues {
                                CType = kitt3nloop_elements_images
                            }
                        }
                    }
                    show := addToList(kitt3nloop_elements_images)
                }
            }
            
            wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        kitt3nloop_elements_slider {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Loop | Elements | Slider
                            description = Create a slider
                            tt_content_defValues {
                                CType = kitt3nloop_elements_slider
                            }
                        }
                    }
                    show := addToList(kitt3nloop_elements_slider)
                }
            }
            
             wizards.newContentElement.wizardItems {
                KITT3N {
                    header = KITT3N
                    after = common,special,menu,plugins,forms
                    elements {
                        kitt3nloop_elements_tabs {
                            iconIdentifier = kitt3n_svg_big
                            title = kitt3n | Loop | Elements | Tabs
                            description = Create tabbed content
                            tt_content_defValues {
                                CType = kitt3nloop_elements_tabs
                            }
                        }
                    }
                    show := addToList(kitt3nloop_elements_tabs)
                }
            }
            
        }'
    );

    // Register for hook to show preview of tt_content element of CType="kitt3nloop_elements*" in page module
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['kitt3nloop_elements'] =
        \KITT3N\Kitt3nLoop\Hooks\Previews\ElementsPreviewRenderer::class;

};

$boot();
unset($boot);