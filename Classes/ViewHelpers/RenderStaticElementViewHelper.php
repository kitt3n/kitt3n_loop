<?php
namespace KITT3N\Kitt3nLoop\ViewHelpers;

/***
 *
 * This file is part of the "kitt3n_layouts" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Return html for column based grid elements
 */
class RenderStaticElementViewHelper extends AbstractViewHelper
{
    /**
     * initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('aElements', 'array', 'Element sarray (Database rows).', true);
        $this->registerArgument('aElement', 'array', 'Element array (Database row).', true);
        $this->registerArgument('aParentData', 'array', 'Parent content element array.', true);
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $sSectionIdentifier = 'elements' . $arguments['aParentData']['uid'];
        $sArticleIdentifier = $sSectionIdentifier . '_element' . $arguments['aElement']['uid'];

        $aHtml = [];

        $sChecked = '';
        if ($arguments['aElements'][0]['uid'] === $arguments['aElement']['uid']) {
            $sChecked = ' checked';
        }

        $aHtml[] = '<input type="radio" name="' . $sSectionIdentifier . '-article-radio" class="article-radio" id="article_' . $sArticleIdentifier . '"' . $sChecked .'/>';
        $aHtml[] = '<article>';

        $aHtml[] = $renderChildrenClosure();

        $aHtml[] = '</article>';

        return implode("", $aHtml);
    }

}