<?php
namespace KITT3N\Kitt3nLoop\ViewHelpers;

/***
 *
 * This file is part of the "kitt3n_loop" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Return html for column based grid elements
 */
class RenderStaticElementsSliderViewHelper extends AbstractViewHelper
{
    /**
     * initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('aElements', 'array', 'Elements array (Database rows).', true);
        $this->registerArgument('oElements', 'array', 'Elements array (Objects).', true);
        $this->registerArgument('aData', 'array', 'Content element array.', true);
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {

        $aHtml = [];
        $aPool = [];
        $sPrevArticleIdentifier = '';
        $sNextArticleIdentifier = '';

        $sSectionIdentifier = 'elements' . $arguments['aData']['uid'];

        $aHtml[] = '<section id="' . $sSectionIdentifier . '" class="tx-kitt3n-loop tx-kitt3n-loop--slider" data-pool="###pool###" data-prev="###prev###" data-next="###next###">';

        if (count($arguments['aElements']) > 1) {

            /*
             * nav
             */
            $aHtml[] = '<nav class="dots">';

            $i = 0;
            foreach ($arguments['aElements'] as $aElement) {

                $sArticleIdentifier = $sSectionIdentifier . '_element' . $aElement['uid'];

                $aHtml[] = '<div class="slide">';
                $aHtml[] = '<input type="radio" name="' . $sSectionIdentifier . '-label-radio" class="label-radio" id="nav_' . $sArticleIdentifier . '" ' . ($i === 0 ? "checked" : "") .' />';
                $aHtml[] = '<label class="nav-label' . ($aElement['nav_icon'] == "" ? "" : " icon icon--" . $aElement['nav_icon']) . '" id="label_' . $sArticleIdentifier . '" for="nav_' . $sArticleIdentifier . '">';
                $aHtml[] = $aElement['nav_title'];
                $aHtml[] = '</label>';
                $aHtml[] = '</div>';

                $aPool[] = 'label_' . $sArticleIdentifier;

                $i++;

            }

            $aHtml[] = '</nav>';

            $sPrevArticleIdentifier .=  'label_' . $sSectionIdentifier .
                '_element' . $arguments['aElements'][count($arguments['aElements'])-1]['uid'];

            $aHtml[] = '<nav class="prev">';
            $aHtml[] = '<a href="javascript:;" class="nav-label-prev" alt="' . $arguments['aElements'][count($arguments['aElements'])-1]['nav_title'] . '" title="' . $arguments['aElements'][count($arguments['aElements'])-1]['nav_title'] . '">';
            $aHtml[] = $arguments['aElements'][count($arguments['aElements'])-1]['nav_title'];
            $aHtml[] = '</a>';
            $aHtml[] = '</nav>';

            $sNextArticleIdentifier .=  'label_' . $sSectionIdentifier .
                '_element' . $arguments['aElements'][1]['uid'];

            $aHtml[] = '<nav class="next">';
            $aHtml[] = '<a href="javascript:;" class="nav-label-next" alt="' . $arguments['aElements'][1]['nav_title'] . '" title="' . $arguments['aElements'][1]['nav_title'] . '">';
            $aHtml[] = $arguments['aElements'][1]['nav_title'];
            $aHtml[] = '</a>';
            $aHtml[] = '</nav>';

        }

        $aHtml[] = '<section>' . $renderChildrenClosure() . '</section>';

        $aHtml[] = '</section>';

        return str_replace(
            ['###pool###', '###prev###', '###next###'],
            [implode(",", $aPool), $sPrevArticleIdentifier, $sNextArticleIdentifier],
            implode("", $aHtml)
        );
    }

}