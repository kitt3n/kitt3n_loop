<?php
namespace KITT3N\Kitt3nLoop\ViewHelpers;

/***
 *
 * This file is part of the "kitt3n_loop" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Return html for column based grid elements
 */
class RenderStaticElementsBoxesViewHelper extends AbstractViewHelper
{
    /**
     * initialize arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('aElements', 'array', 'Elements array (Database rows).', true);
        $this->registerArgument('oElements', 'array', 'Elements array (Objects).', true);
        $this->registerArgument('aData', 'array', 'Content element array.', true);
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {

        $aHtml = [];

        $sSectionIdentifier = 'elements' . $arguments['aData']['uid'];

        $aHtml[] = '<section id="' . $sSectionIdentifier . '" class="tx-kitt3n-loop tx-kitt3n-loop--boxes">';

        $aHtml[] = '<section>' . $renderChildrenClosure() . '</section>';

        $aHtml[] = '</section>';

        return implode("", $aHtml);
    }

}