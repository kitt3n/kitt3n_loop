<?php
namespace KITT3N\Kitt3nLoop\DataProcessing;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;

use TYPO3\CMS\Frontend\Service\TypoLinkCodecService;

/**
 * Class for data processing for the content element "My new content element"
 */
class Kitt3nLoopElementProcessor implements DataProcessorInterface
{
    /**
     * lazyImageService
     *
     * @var \KITT3N\Kitt3nImage\Service\LazyImageService
     * @inject
     */
    protected $lazyImageService = null;

    /**
     * elementRepository
     *
     * @var \KITT3N\Kitt3nLoop\Domain\Repository\ElementRepository
     * @inject
     */
    protected $elementRepository = null;

    /**
     * Inject the $articleRepository repository
     * (@inject does not seem to work)
     */
    public function manuallyInjectElementRepository()
    {
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->elementRepository = $objectManager->get('KITT3N\\Kitt3nLoop\\Domain\\Repository\\ElementRepository');
        /*
         * setDefaultQuerySettings()
         */

        $querySettings = $objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(false);
        $this->elementRepository->setDefaultQuerySettings($querySettings);
    }

    /**
     * Process data for the content element "My new content element"
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $processedData['foo'] = 'This variable will be passed to Fluid';

        $table = 'tx_kitt3nloop_domain_model_element';
        $joinTable = 'ttcontent_tx_kitt3nloop_domain_model_element_mm';

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($table);
        
        $statement = $queryBuilder
        ->select('*')
        ->from($table)
        ->join(
            $table,
            $joinTable,
            'elementMM',
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq(
                    'elementMM.uid_local', $processedData['data']['uid']
                ),
                $queryBuilder->expr()->eq(
                    'elementMM.uid_foreign',
                    $queryBuilder->quoteIdentifier($table . '.uid')
                )
            )
        )
        ->where(
            $queryBuilder->expr()->eq($table .'.deleted', 0),
            $queryBuilder->expr()->eq($table .'.hidden', 0)
        )
        ->orderBy('elementMM.sorting', 'ASC');

        $processedData['aElements'] = $statement->execute()->fetchAll();

        $processedData['oElements'] = [];
        $processedData['oElementsImagesHtml'] = [];
        if (is_array($processedData['aElements']) and count($processedData['aElements']) > 0) {

            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
            $this->lazyImageService = $objectManager->get('KITT3N\\Kitt3nImage\\Service\\LazyImageService');

            $this->manuallyInjectElementRepository();
            foreach($processedData['aElements'] as $iKey => $aElement) {

                /*
                 * get element object
                 */
                $processedData['oElements'][] = $this->elementRepository->findByUid($aElement['uid']);

                /*
                 * process image => get html
                 */
                $oImage = $processedData['oElements'][count($processedData['oElements'])-1]->getImage();

                if ($oImage !== null) {
                    $processedData['aElements'][$iKey]['processedImage'] = $this->lazyImageService->returnLazyImageHtml(
                        [
                            'coreFileReference' => $oImage,
                            'fileReferenceUid' => $oImage->getUid()
                        ]
                    );
                } else {
                    $processedData['aElements'][$iKey]['processedImage'] = null;
                }

                /*
                 * process link -> get uri, title, target
                 */
                $sLink = $processedData['oElements'][count($processedData['oElements'])-1]->getLink();

                if ($sLink !== '') {
                    $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
                    $typoLinkCodec = GeneralUtility::makeInstance(TypoLinkCodecService::class);
                    $typolinkConfiguration = $typoLinkCodec->decode($sLink);
                    $processedData['aElements'][$iKey]['processedLink'] = $typolinkConfiguration;
                    $processedData['aElements'][$iKey]['processedLink']['url'] = $contentObject->typoLink_URL(
                        [
                            'parameter' => $typoLinkCodec->encode($typolinkConfiguration)
                        ]
                    );
                }

            }
        }

        return $processedData;
    }
}
