function txKitt3nLoop__findAncestor (el, cls) {
    while ((el = el.parentNode) && el.className.indexOf(cls) < 0);
    return el;
}

function txKitt3nLoop__arraySearch(arr,val,key) {

    for (var i=0; i<arr.length; i++) {
        if (arr[i] === val) {

            switch (true) {
                case key === "prev":

                    if (i === 0) {
                        return (arr.length)-1;
                    }
                    return i-1;

                    break;
                case key === "next":

                    if (arr.length > i+1) {
                        return i+1;
                    }
                    return 0;

                    break;
                default:
                    return i;
            }

        }
    }

    return false;

}

var txKitt3nLoop__aLabels = document.getElementsByClassName('nav-label');
var txKitt3nLoop__aLabelsPrev = document.getElementsByClassName('nav-label-prev');
var txKitt3nLoop__aLabelsNext = document.getElementsByClassName('nav-label-next');


for (var i = 0; i < txKitt3nLoop__aLabels.length; i++) {
    txKitt3nLoop__aLabels[i].addEventListener('click', function(){

        var attrFor = this.getAttribute('for');
        var idToTrigger = attrFor.replace('nav', 'article');
        document.getElementById(idToTrigger).click();
        if (typeof myLazyLoad !== 'undefined') {
            myLazyLoad.update();
        }

        if (txKitt3nLoop__aLabelsPrev.length > 0 && txKitt3nLoop__aLabelsNext.length > 0) {

            oTxKitt3nLoop = txKitt3nLoop__findAncestor(this, 'tx-kitt3n-loop');
            var oAPrev = oTxKitt3nLoop.getElementsByClassName('nav-label-prev');
            var oANext = oTxKitt3nLoop.getElementsByClassName('nav-label-next');

            var sAttrDataPool = oTxKitt3nLoop.getAttribute('data-pool');
            var aAttrDataPool = sAttrDataPool.split(",");

            /*
             * Set data attributes for prev and next
             */
            var iAttrDataPoolPrev = txKitt3nLoop__arraySearch(aAttrDataPool, attrFor.replace('nav', 'label'), "prev");
            oTxKitt3nLoop.setAttribute('data-prev', aAttrDataPool[iAttrDataPoolPrev]);
            var iAttrDataPoolNext = txKitt3nLoop__arraySearch(aAttrDataPool, attrFor.replace('nav', 'label'), "next");
            oTxKitt3nLoop.setAttribute('data-next', aAttrDataPool[iAttrDataPoolNext]);

            /*
             * Set labels for prev and next
             */
            var oLabelPrev = document.getElementById(aAttrDataPool[iAttrDataPoolPrev]);
            var sLabelPrevText = oLabelPrev.innerHTML;
            oAPrev[0].innerHTML = sLabelPrevText;
            var oLabelNext = document.getElementById(aAttrDataPool[iAttrDataPoolNext]);
            var sLabelNextText = oLabelNext.innerHTML;
            oANext[0].innerHTML = sLabelNextText;

        }

    });
}

if (txKitt3nLoop__aLabelsPrev.length > 0 && txKitt3nLoop__aLabelsNext.length > 0) {

    for (var i = 0; i < txKitt3nLoop__aLabelsPrev.length; i++) {
        txKitt3nLoop__aLabelsPrev[i].addEventListener('click', function(){

            var oTxKitt3nLoop = txKitt3nLoop__findAncestor(this, 'tx-kitt3n-loop');
            var oANext = oTxKitt3nLoop.getElementsByClassName('nav-label-next');

            var sAttrDataPrev = oTxKitt3nLoop.getAttribute('data-prev');
            var sAttrDataPool = oTxKitt3nLoop.getAttribute('data-pool');
            var aAttrDataPool = sAttrDataPool.split(",");

            document.getElementById(sAttrDataPrev).click();
            if (typeof myLazyLoad !== 'undefined') {
                myLazyLoad.update();
            }

            /*
             * Set data attributes for prev and next
             */
            var iAttrDataPoolPrev = txKitt3nLoop__arraySearch(aAttrDataPool, sAttrDataPrev, "prev");
            oTxKitt3nLoop.setAttribute('data-prev', aAttrDataPool[iAttrDataPoolPrev]);
            var iAttrDataPoolNext = txKitt3nLoop__arraySearch(aAttrDataPool, sAttrDataPrev, "next");
            oTxKitt3nLoop.setAttribute('data-next', aAttrDataPool[iAttrDataPoolNext]);

            /*
             * Set labels for prev and next
             */
            var oLabelPrev = document.getElementById(aAttrDataPool[iAttrDataPoolPrev]);
            var sLabelPrevText = oLabelPrev.innerHTML;
            this.innerHTML = sLabelPrevText;
            var oLabelNext = document.getElementById(aAttrDataPool[iAttrDataPoolNext]);
            var sLabelNextText = oLabelNext.innerHTML;
            oANext[0].innerHTML = sLabelNextText;

        });
    }

    for (var i = 0; i < txKitt3nLoop__aLabelsNext.length; i++) {
        txKitt3nLoop__aLabelsNext[i].addEventListener('click', function(){

            var oTxKitt3nLoop = txKitt3nLoop__findAncestor(this, 'tx-kitt3n-loop');
            var oAPrev = oTxKitt3nLoop.getElementsByClassName('nav-label-prev');

            var sAttrDataNext = oTxKitt3nLoop.getAttribute('data-next');
            var sAttrDataPool = oTxKitt3nLoop.getAttribute('data-pool');
            var aAttrDataPool = sAttrDataPool.split(",");

            document.getElementById(sAttrDataNext).click();
            if (typeof myLazyLoad !== 'undefined') {
                myLazyLoad.update();
            }

            /*
             * Set data attributes for prev and next
             */
            var iAttrDataPoolPrev = txKitt3nLoop__arraySearch(aAttrDataPool, sAttrDataNext, "prev");
            oTxKitt3nLoop.setAttribute('data-prev', aAttrDataPool[iAttrDataPoolPrev]);
            var iAttrDataPoolNext = txKitt3nLoop__arraySearch(aAttrDataPool, sAttrDataNext, "next");
            oTxKitt3nLoop.setAttribute('data-next', aAttrDataPool[iAttrDataPoolNext]);

            /*
             * Set labels for prev and next
             */
            var oLabelPrev = document.getElementById(aAttrDataPool[iAttrDataPoolPrev]);
            var sLabelPrevText = oLabelPrev.innerHTML;
            oAPrev[0].innerHTML = sLabelPrevText;
            oAPrev[0].setAttribute('alt', sLabelPrevText);
            oAPrev[0].setAttribute('title', sLabelPrevText);
            var oLabelNext = document.getElementById(aAttrDataPool[iAttrDataPoolNext]);
            var sLabelNextText = oLabelNext.innerHTML;
            this.innerHTML = sLabelNextText;
            this.setAttribute('alt', sLabelNextText);
            this.setAttribute('title', sLabelNextText);

        });
    }

}




