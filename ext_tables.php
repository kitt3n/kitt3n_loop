<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kitt3n_loop', 'Configuration/TypoScript', 'kitt3n | Loop');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nloop_domain_model_element', 'EXT:kitt3n_loop/Resources/Private/Language/locallang_csh_tx_kitt3nloop_domain_model_element.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nloop_domain_model_element');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder