#
# Table structure for table 'tx_kitt3nloop_domain_model_element'
#
CREATE TABLE tx_kitt3nloop_domain_model_element (

	nav_title varchar(255) DEFAULT '' NOT NULL,
	nav_icon varchar(255) DEFAULT '' NOT NULL,
	header varchar(255) DEFAULT '' NOT NULL,
	subheader varchar(255) DEFAULT '' NOT NULL,
	text text,
	image int(11) unsigned NOT NULL default '0',
	icon varchar(255) DEFAULT '' NOT NULL,
	link text,
	variant varchar(255) DEFAULT '' NOT NULL,
	date int(11) DEFAULT '0' NOT NULL,
	date1 int(11) DEFAULT '0' NOT NULL,

);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
   tx_kitt3nloop_domain_model_element int(11) DEFAULT '0' NOT NULL,
   tx_kitt3nloop_template varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'ttcontent_tx_kitt3nloop_domain_model_element_mm'
#
CREATE TABLE ttcontent_tx_kitt3nloop_domain_model_element_mm (
   uid_local int(11) DEFAULT '0' NOT NULL,
   uid_foreign int(11) DEFAULT '0' NOT NULL,
   sorting int(11) DEFAULT '0' NOT NULL,
   KEY uid_local (uid_local),
   KEY uid_foreign (uid_foreign)
);